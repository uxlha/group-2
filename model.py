import torch.nn as nn
import torch.nn.functional as F
import pickle


class SimpleNN(nn.Module):
    """
    A simple neural network model for multi-class classification.

    Args:
        input_size (int): The size of the input features.
        num_classes (int): The number of classes for classification.
    """

    def __init__(self, input_size, num_classes):
        super(SimpleNN, self).__init__()
        self.norm = nn.InstanceNorm1d(3)  # x,y,z dimension
        self.fc1 = nn.Linear(input_size, 128)
        self.fc2 = nn.Linear(128, 64)
        self.fc3 = nn.Linear(64, num_classes)

    def forward(self, x):
        """
        Forward pass of the neural network. In this method, the input tensor x is processed through the layers of the network to produce an output tensor.

        The forward pass consists of the following steps:

        Reshaping the input tensor: The input tensor x is reshaped using the view method. It is reshaped to have dimensions (-1, 21, 3), which means that the tensor is transformed to have a batch size of any value, 21 rows (representing the number of hand landmarks), and 3 columns (representing the x, y, and z coordinates of each landmark).

        Permuting the dimensions: After reshaping, the dimensions of the tensor are permuted using the permute method. The dimensions are rearranged to (0, 2, 1), which means that the first dimension (batch size) remains unchanged, the second dimension (number of features) becomes the third dimension, and the third dimension (number of landmarks) becomes the second dimension. This rearrangement is done to align the tensor with the expected input shape of subsequent layers.

        Normalization: The normalized tensor x is obtained by passing it through the norm function. This function applies instance normalization to the input tensor along the second dimension (dim=1). Instance normalization normalizes the input tensor independently for each sample in the batch, which can help improve the convergence and stability of the network during training.

        Flattening the tensor: The tensor x is flattened using the view method. It is reshaped to have a size of (batch_size, -1), where -1 indicates that the size of that dimension is inferred based on the other dimensions. This flattening operation is necessary to prepare the tensor for the fully connected layers.

        First hidden layer: The flattened tensor x is passed through the first fully connected layer fc1. The output of this layer is obtained by applying the ReLU activation function (F.relu) to the input tensor.

        Second hidden layer: The output of the first hidden layer is then passed through the second fully connected layer fc2. Again, the ReLU activation function is applied to the input tensor.

        Output layer: The output of the second hidden layer is passed through the output layer fc3. Unlike the previous layers, no activation function is applied to the output of this layer.

        Softmax activation: The output tensor is then passed through the softmax activation function (F.softmax) along the second dimension (dim=1). This function converts the output values into probabilities, representing the predicted class probabilities for each input sample.

        Returning the output: The final output tensor, representing the predicted class probabilities, is returned from the forward method.

        The forward pass is a fundamental step in training and using neural networks. It propagates the input data through the network's layers, applying transformations and activations to produce an output that can be used for further processing or evaluation.

        Args:
            x (torch.Tensor): Input tensor of shape (batch_size, input_size).

        Returns:
            torch.Tensor: Output tensor of shape (batch_size, num_classes).

        """
        # Reshape the tensor to contain the x,y,z coordinates of the 21 hand landmarks
        x = x.view(-1, 21, 3).permute(0, 2, 1)  # (n_samples, n_dim, n_features)
        x = self.norm(x)
        x = x.view(x.size(0), -1)
        x = F.relu(self.fc1(x))  # ReLU activation for the first hidden layer
        x = F.relu(self.fc2(x))  # ReLU activation for the second hidden layer
        x = self.fc3(x)  # Output layer without activation function
        x = F.softmax(x, dim=1)  # Softmax activation for multi-class classification
        return x


class ModelFactory:
    @staticmethod
    def load_model(model_path):
        """
        Loads a trained model from a file.

        Args:
            model_path (str): The path to the model file.

        Returns:
            SimpleNN: The loaded model.

        Raises:
            FileNotFoundError: If the specified model file does not exist.
            IOError: If there is an error reading the model file.
        """
        with open(model_path, "rb") as file:
            model_dict = pickle.load(file)
        model_state = model_dict["model"]

        # Initialize the model architecture
        input_size = model_state["fc1.weight"].size(1)
        num_classes = model_state["fc3.bias"].size(0)
        model = SimpleNN(input_size, num_classes)
        model.load_state_dict(model_state)

        return model
