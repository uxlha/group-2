# 💻 Powerpoint Control

[![Page](https://img.shields.io/website?url=https%3A%2F%2Fm-j-w-f.github.io%2Faiss_cv_docs&label=Documentation%20Website&style=flat&logo=github)](https://m-j-w-f.github.io/aiss_cv_docs/)

Find the documentation [here](https://m-j-w-f.github.io/aiss_cv_docs/)

## Prerequisites

Before running the scripts, make sure you have Python installed. You also need to install the required packages listed in the `requirements.txt` file.

### Install Required Packages

1. Install requirements from the file (recommended to do this in a venv)

    ```
    pip install -r requirements.txt
    ```

## For Headless Use Over SSH (Jetson)

To run the script `final_socket_nano.py` on a remote server (like a Jetson) in a headless environment, follow these steps:

1. Open a terminal.
2. SSH into your remote server.
3. Navigate to the directory containing the script.
4. Execute the following command (to handle issues with mediapipe requiring a display):

    ```
    export DISPLAY=:0 && DRI_PRIME=1 python3 /final_socket_nano.py
    ```

## Run File on Laptops

To run the script `socket_laptop_final.py` on your laptop:

1. Adapt the path to your powerpoint
2. Excecute the file, when asked for input press enter to start presentation mode

    ```
    python3 /socket_laptop_final.py
    ```

