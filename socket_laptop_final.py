import pyautogui
import socket
import cv2
import numpy as np
import powerpoint


# PowerPoint control functions
def deserialize_landmarks(data_str):
    """
    Converts a string of comma-separated values back into a list of floats representing landmarks.

    Args:
        data_str (str): String of comma-separated values.

    Returns:
        data_list (list): List of floats representing the landmarks.
    """
    try:
        if data_str != "no_hand":
            data_list = list(map(float, data_str.split(",")))
        else:
            data_list = data_str
    except:
        data_list = data_str

    return data_list


def draw_hand_keypoints(keypoints, frame_size=(160, 120), margin=10):
    """
    Draws hand keypoints and connections on a transparent background image using Mediapipe.

    Args:
        keypoints (list): List of floats representing hand keypoints.
        frame_size (tuple): Size of the frame (height, width).
        margin (int): Margin around the keypoints to leave space.

    Returns:
        img (numpy.array): Image with keypoints and connections drawn.
    """

    img = np.ones((frame_size[0], frame_size[1], 4), dtype=np.uint8) * 255
    img[:, :, 3] = 0  # Transparent background

    try:
        # Calculate scaling factor to fit keypoints within the frame with margins
        max_x = max(keypoints[::3])
        min_x = min(keypoints[::3])
        max_y = max(keypoints[1::3])
        min_y = min(keypoints[1::3])

        # Calculate dimensions of the gesture
        gesture_width = max_x - min_x
        gesture_height = max_y - min_y

        # Determine the scaling factor based on the larger dimension
        if gesture_width > gesture_height:
            scale_factor = (frame_size[1] - 2 * margin) / gesture_width
        else:
            scale_factor = (frame_size[0] - 2 * margin) / gesture_height

        # Adjust keypoints coordinates
        scaled_keypoints = []
        for i in range(0, len(keypoints), 3):
            x = int((keypoints[i] - min_x) * scale_factor) + margin
            y = int((keypoints[i + 1] - min_y) * scale_factor) + margin
            scaled_keypoints.append((x, y))

        # Define connections between keypoints as pairs of indices
        connections = [
            (0, 1), (1, 2), (2, 3), (3, 4),  # Thumb
            (0, 5), (5, 6), (6, 7), (7, 8),  # Index finger
            (0, 9), (9, 10), (10, 11), (11, 12),  # Middle finger
            (0, 13), (13, 14), (14, 15), (15, 16),  # Ring finger
            (0, 17), (17, 18), (18, 19), (19, 20),  # Little finger
            (5, 9), (9, 13), (13, 17),  # Connections between fingers
        ]

        # Draw connections between keypoints in grey
        for connection in connections:
            start_idx, end_idx = connection
            if 0 <= start_idx < len(scaled_keypoints) and 0 <= end_idx < len(scaled_keypoints):
                start_point = scaled_keypoints[start_idx]
                end_point = scaled_keypoints[end_idx]
                # Draw grey lines for connections
                color = (150, 150, 150, 255)  # Grey (R, G, B, A)
                cv2.line(img, start_point, end_point, color, 3)  # Thick lines for connections

        # Draw circles for keypoints in black
        for point in scaled_keypoints:
            cv2.circle(img, point, 5, (0, 0, 0, 255), -1)  # Black circles for keypoints

    except (IndexError, ValueError, TypeError) as e:
        pass

    return img


def main():
    """
    This function is the entry point of the program on the client (Laptop).
    It connects to a server, receives data, and performs actions based on the received data.

    Server Configuration:

    * SERVER_HOST: The IP address of the server.
    * SERVER_PORT: The port number of the server.

    Presentation Configuration:

    * PRESENTATION_PATH: The path to the PowerPoint presentation file.

    Mouse Movement Configuration:

    * DISPLAY_WIDTH: The width of the display screen.
    * DISPLAY_HEIGHT: The height of the display screen.

    Actions:

    * Connects to the server using a TCP socket.
    * Receives data from the server.
    * Deserializes the received data.
    * Draws hand keypoints on a white background.
    * Displays the image with hand keypoints.
    * Performs different actions based on the predicted gesture:
        * If the gesture is "laser pointer" or "reposition laser", activates the laser pointer and moves the mouse to the laser pointer position on the screen.
        * If the gesture is "deactivate laser pointer", deactivates the laser pointer.
        * If the gesture is "swipe_left", goes to the previous slide.
        * If the gesture is "stop", holds the presentation.
        * If the gesture is "swipe_right", goes to the next slide.
    * Waits for a key press and closes the window if 'q' is pressed.
    * Releases resources and closes all windows.

    Returns:
        None
    """

    # Server configuration
    SERVER_HOST = "192.168.0.100"
    SERVER_PORT = 12345

    # Path to the PowerPoint presentation
    PRESENTATION_PATH = r"C:\Users\jonas\Downloads\AISS_CV_SS24_Group2_visuals.pptx"

    # Start PowerPoint and open the presentation
    pp = powerpoint.Presentation(PRESENTATION_PATH)

    pp.start_powerpoint()

    # Wait for user input to start presentation mode
    input("Press Enter to start presentation mode...")
    pp.start_presentation_mode()

    # Get display size for mouse movement
    DISPLAY_WIDTH, DISPLAY_HEIGHT = pyautogui.size()

    # Connect to the server
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:
        client_socket.connect((SERVER_HOST, SERVER_PORT))

        while True:
            # Receive data from the server
            received_data = client_socket.recv(1024).decode("utf-8").split("/")
            if not received_data:
                continue

            if len(received_data) != 4:
                continue

            predicted_gesture = received_data[0]
            hand_keypoint_x = float(received_data[1])
            hand_keypoint_y = float(received_data[2])
            full_keypoints = received_data[3]

            if predicted_gesture != "none":
                print(f"Detected gesture {predicted_gesture}")

            # Deserialize landmarks
            hand_keypoints = deserialize_landmarks(full_keypoints)

            # Draw keypoints on a white background
            img = draw_hand_keypoints(hand_keypoints)

            # Create a named window with WINDOW_NORMAL flag
            cv2.namedWindow("Hand Keypoints", cv2.WINDOW_NORMAL)

            # Set window position and size
            cv2.moveWindow("Hand Keypoints", 30, 30)
            cv2.resizeWindow("Hand Keypoints", 160, 120)
            cv2.setWindowProperty("Hand Keypoints", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
            # Display the image
            cv2.imshow("Hand Keypoints", img)

            # Handle different gestures
            if predicted_gesture == "laser pointer" or predicted_gesture == "reposition laser":
                if predicted_gesture == "laser pointer":
                    pp.activate_laser_pointer(True)

                # Move mouse to laser pointer position on screen
                if 0 <= hand_keypoint_x <= 1 and 0 <= hand_keypoint_y <= 1:
                    pyautogui.moveTo(int(hand_keypoint_x * DISPLAY_WIDTH), int(hand_keypoint_y * DISPLAY_HEIGHT))

            elif predicted_gesture == "deactivate laser pointer":
                pp.activate_laser_pointer(False)

            elif predicted_gesture == "swipe_left":
                pp.previous_slide()

            elif predicted_gesture == "swipe_right":
                pp.next_slide()

            elif predicted_gesture == "stop":
                pp.hold_presentation()

            # Wait for a key press (milliseconds), and close the window if 'q' is pressed
            if cv2.waitKey(1) & 0xFF == ord("q"):
                break

    # Release resources and close all windows
    cv2.destroyAllWindows()
    pp.end_presentation()


if __name__ == "__main__":
    main()
