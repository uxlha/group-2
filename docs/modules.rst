.. _modules:

Code
====
Below is a comprehensive list of all modules included in our documentation.

.. toctree::
   :maxdepth: 1

   data_loading
   final_socket_nano
   generate_images
   inference
   model
   powerpoint
   socket_laptop_final
   train_model
