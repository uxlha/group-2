# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html
import os
import sys

sys.path.insert(0, os.path.abspath(".."))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "PPControl"
copyright = "2024, Darius Kiabi, Moritz Feik, Jonas Michel, Joschka Feilhauer, Johannes Reber\u200b"
author = "Darius Kiabi, Moritz Feik, Jonas Michel, Joschka Feilhauer, Johannes Reber\u200b"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx.ext.viewcode",
    "sphinxcontrib.bibtex",
    "sphinx_design",
    "sphinx_fontawesome",
]
napoleon_google_docstring = True
napoleon_numpy_docstring = True
napoleon_use_ivar = False
napoleon_use_param = True  # Disable napoleon's param parsing
napoleon_use_rtype = False  # Set to False to prevent extra return type section
napoleon_custom_sections = [("Returns", "params_style")]

bibtex_bibfiles = ["text/refs.bib"]

# Add your missing package names to this list
autodoc_mock_imports = ["win32com"]

templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", "DATA/", "archive"]

html_css_files = ["https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css", "custom.css"]


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "pydata_sphinx_theme"
html_static_path = ["_static"]

html_title = "PPC"
html_logo = "_static/logo.svg"
html_theme_options = {
    # "navbar_align": "left", # Comment out for center
    "gitlab_url": "https://gitlab.kit.edu/uxlha/group-2",
    "logo": {
        "text": "PPControl",
        "image_light": "_static/logo.svg",
        "image_dark": "_static/logo.svg",
    },
}

html_sidebars = {"text/*": []}


def setup(app):
    app.connect("html-page-context", hide_secondary_sidebar)


def hide_secondary_sidebar(app, pagename, templatename, context, doctree):
    if pagename == "index":
        context["sidebar_secondary"] = ""
