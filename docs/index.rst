.. PPControl documentation master file, created by
   sphinx-quickstart on Tue Jul 16 16:13:52 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

:html_theme.sidebar_secondary.remove:

.. raw:: html

    <style>
    .icon-blue { color: #4e79a7; }
    .icon-orange { color: #f28e2b; }
    .icon-green { color: #59a14f; }
    .icon-red { color: #e15759; }
    .card-header-custom {
        padding: 0.75rem 0 0.75rem 0.75rem;  /* Removed left padding */
        margin-bottom: 0;
    }
    .header-content {
        display: flex;
        align-items: center;
    }
    .header-content i {
        margin-right: 0;  /* Removed right margin */
        font-size: 1.25em;
        padding-right: 0.5rem;  /* Keep some space between icon and text */
    }
    .header-content span {
        padding-left: 0.5rem;  /* Keep some space between icon and text */
    }
    .sd-card-hover:hover {
        transform: translateY(-2px);
        box-shadow: 0 .5rem 1rem rgba(0,0,0,.15);
    }
    </style>

PPControl documentation
=======================

Welcome to PPControl's documentation!

Navigate through the table below or just follow the **Next** links at the bottom of each page for an easy walkthough.

.. grid:: 2 2 2 2
    :gutter: 2

    .. grid-item-card::
        :link: text/intro
        :link-type: doc
        :class-card: sd-card-hover

        .. raw:: html
            
            <div class="card-header-custom">
                <div class="header-content">
                    <i class="fas fa-book-open icon-green"></i>
                    <span><strong>Introduction</strong></span>
                </div>
            </div>

        ^^^
        Get started with an overview of our project and its goals.

    .. grid-item-card::
        :link: text/model_selection
        :link-type: doc
        :class-card: sd-card-hover

        .. raw:: html
            
            <div class="card-header-custom">
                <div class="header-content">
                    <i class="fas fa-chart-line icon-blue"></i>
                    <span><strong>Model Selection</strong></span>
                </div>
            </div>

        ^^^
        Learn which model architecture we choose and why.

    .. grid-item-card::
        :link: text/model_implementation
        :link-type: doc
        :class-card: sd-card-hover

        .. raw:: html
            
            <div class="card-header-custom">
                <div class="header-content">
                    <i class="fas fa-cogs icon-orange"></i>
                    <span><strong>Model Implementation</strong></span>
                </div>
            </div>

        ^^^
        Discover how we implemented our model and how each component works together.

    .. grid-item-card::
        :link: modules
        :link-type: doc
        :class-card: sd-card-hover

        .. raw:: html
            
            <div class="card-header-custom">
                <div class="header-content">
                    <i class="fas fa-code icon-red"></i>
                    <span><strong>Modules</strong></span>
                </div>
            </div>

        ^^^
        Explore our project's code and its functionalities.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   text/intro
   text/model_selection
   text/model_implementation
   text/conclusion

.. toctree::
   :maxdepth: 1
   :caption: Modules and Scripts:

   modules

Indices and tables
------------------
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

