.. _intro:

Introduction
============
Our idea for an object detection task is to streamline the controls for slide-based presentations.
Current controls feel interruptive and outdated. When trying to share a topic with an audience, the slides should act complementary.
In a study conducted by :footcite:t:`cao2005evaluation`, “bare hand”-controls were preferred by listeners in all 4 categories: Overall, Clearness, Efficiency, and Attractiveness.
Most presenters prefer to stay behind their laptops already; thus, gesturing to emphasize contents on the screen seemed natural.
Gesture controls would also allow for more eye contact. :footcite:t:`fourney2010gesturing` further emphasize the appeal of this way of storytelling.
By analyzing the gesturing used in Google Tech Talks, they derived that gesturing certain actions like pointing to signal a transition is already common practice.
In addition, our idea of a Gesture-Controlled Presentation System also coincides with the long goals of HCI (Human-Computer-Interaction) “to migrate natural means that [humans use] to interact with each other” :footcite:p:`sharma2015human`.

With these learnings, we aim to deliver a system that will set the foundation for an accessible and iterable presentation style.
In the future, the Computer Vision model could be run on conventional hardware, like a laptop.
Gestures will be predefined. The system will keep monitoring your hands (:ref:`Figure 1 <Sketch>`).
Once it detects a gesture, it will activate a set command.
For our purpose, we will use PowerPoint by Microsoft as it’s the most used presentation program to date :footcite:p:`osipovskaya2019presentation`. 

.. figure:: img/Sketch.png
   :name: Sketch
   :align: center
   :class: custom-max-width-50
   
   Figure 1: Gesture Controlled Presentation System


.. footbibliography::