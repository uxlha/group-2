.. _model_selection:

Model Selection
===============

There are multiple factors that came into play to decide what approach to take.
Firstly, there is the time component.
There was only one semester to tackle this task, so decisions were made based on implementation difficulty.
Next, we have the limitations of the Jetson Nano :footcite:p:`noauthor_jetson_nodate`.
Considering we want the use of gestures to feel natural, interference speed should be minimal to allow for a lot of frames-per-second (FPS).
So, the model should require little computing power.
Ideally, little training data should be necessary to achieve good results as the training data images would have to be collected manually.

Before looking at potential candidates, we had to solidify our idea of what we wanted to implement.
Early on, we recognized that we had to split the task into hand detection and gesture classification.
This way, the gesture classifier can focus on the prevalent part of the image, reducing computational effort and simultaneously increasing accuracy.
For hand detection, we aimed for a precise model.
If the hand couldn’t be detected consistently and in a uniform manner, the classification model could struggle to output reliable predictions.
Gesture classification, on the other hand, needs to be confident.
We want it to work at a wide range of distances from the camera.
Depth and distance determination must be possible to identify the active presenter’s hand.
Moreover, we opted for static gestures. This means that gestures only consist of one single hand posture and could technically be detected in a single frame.
Dynamic gestures, where you would have to learn a defined movement sequence, would dramatically increase the project's complexity.
There was also the fear that dynamic gestures would disrupt the natural flow of gesturing too much.
Besides, it would also create an entry barrier for new users, moving away from the idea of intuitive controls.
In addition, we decided on one-handed gestures. This will simplify training and lets us classify each hand independently.
Parallel to static gestures, this will also minimize the constraints put on the presenter. 
Moving on, false positives in the classification step would ruin the whole appeal of a hand-controlled style of presentation.
For our proof-of-concept, we decided on several gestures to be implemented (:ref:`Figure 2 <Gestures>`).: 

.. figure:: img/Gestures.png
   :align: center
   :name: Gestures
   :class: custom-max-width-75
   
   Figure 2: Overview Gestures

Firstly, being able to move slides is integral to PowerPoint controls.
To choose a gesture for each command, we asked ourselves what the user would try to do if they were only given the list of actions.
Intuitively, a pointing or sliding action seemed fit when trying to move to the next slide.
Recaptivating what was said above we can derive the use of a pointing action to be suitable.
In outlook to the next gesture, pointing left/right with your index finger and thumb spreading upwards is deemed appropriate.
For all our gestures, we decided that fingers not actively used or explicitly defined for an action are to be held together and inward.
Especially the thumb position is to be emphasized. Next, to fulfill basic requirements, we had to implement a way to highlight.
Again, if you want to point at something, use your index finger. We allocate an index finger pointing up to the laser pointer action.
Once detected you can move your hand in this pose to move the laser pointer on screen.
To round things off, we assign a halt sign to end the presentation mode. Re-using this gesture would start the presentation mode again.
Each gesture can be used with either hand.
Also, any gesture can be upheld but the same action will only be activated again after a little timeframe has passed.
Lastly, gestures were defined to be intuitive for the presenter, so for the listener directions might seem inverted.

Two step-approach using a YOLOv8 Nano and MobileNet
---------------------------------------------------

Our first idea was to implement a fine-tuned YOLOv8 Nano :footcite:p:`Jocher_Ultralytics_YOLO_2023` model to detect the hands of the current presenter.
YOLOv8 nano is marketed as lightweight and efficient, coinciding with the idea of a real-time object task on edge devices like the Jetson Nano.
The resulting bounding box would be used to crop the image. It would then be fed into a custom-trained MobileNet :footcite:p:`DBLP:journals/corr/HowardZCKWWAA17` to classify gestures.
MobileNet is a convolutional neural network meant for mobile applications.
This approach would leverage the strengths of both models to achieve efficient hand gesture recognition on a resource-constrained device like the Jetson Nano.
Taking a closer look, even when using the lightweight YOLOv8 nano out of all YOLOv8 models, it involves significant computational resources for real-time use, yielding only single-digit FPS performance and potential performance bottlenecks.
In addition, we were able to ascertain that the resulting bounding boxes lacked precision.
They would include irrelevant background information or even cut off thumbs out of frame, negatively affecting the subsequent gesture classification accuracy.
Moreover, fine-tuning the model for hand detection requires a significant amount of labeled training data and careful parameter tuning.
With the concern that the resulting model would not generalize well to varying lighting conditions, hand sizes, or skin tones without extensive training, we had to forgo the idea.
Furthermore, initial tests of our trained YOLOv8 Nano and MobileNet model showed that this approach was indeed performing poorly in regard to inference speed on the Jetson Nano.

Better approach: MediaPipe landmark detection with neural network classifier
----------------------------------------------------------------------------

The second approach starts off with Mediapipe :footcite:p:`lugaresi_mediapipe_2019`.
Mediapipe is a framework developed by Google that provides a variety of pre-trained models for various tasks, including hand landmark detection.
Unlike YOLO it doesn’t output bounding boxes but a set of nodes that represent the current position of certain landmarks of the user’s hand.
These landmarks can be used to determine the current gesture using a classifier.
We initially employed a Random Forest model to classify static gestures based on single images. We then experimented with an LSTM model for dynamic gesture recognition, using a window of 10 consecutive images to capture temporal dependencies. However, the LSTM did not work out due to its higher complexity, requiring more training data and often focusing on irrelevant details within the image sequence. Additionally, package dependencies on the Jetson Nano prevented the Random Forest model from working properly. Ultimately, we found that a simple Neural Network provided the best balance of performance and efficiency, achieving reliable gesture recognition.
Compared to the YOLOv8 Nano and MobileNet approach, the MediaPipe and neural network approach is more lightweight and efficient.
Instead of running two computationally expensive models in sequence, we only need to run the hand landmark detection model and a very lightweight neural net.
Additonally, the training of the downstream neural network classifier is less complex and requires less labeled data, since the hand landmarks are already extracted and there is no need to use an image-based model.
Furthermore, we can precisely determine certain features of the hand, such as the index finger, used for the control of the laser pointer.


.. footbibliography::
