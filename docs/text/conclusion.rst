Conclusion
==========
The implemented approach demonstrates a proof of concept for using "bare hand" controls, which eliminates the need for additional devices such as presenters or clickers.
This innovative method leverages hand gestures to interact with and control presentation software, offering a hands-free and intuitive user experience.

The system is designed to operate in real-time, providing immediate responses to hand gestures.
This real-time capability is crucial for maintaining the flow of a presentation without noticeable lag.
The system's ability to make accurate predictions ensures that gestures are correctly interpreted, which enhances the reliability and usability of the tool during live presentations.

One significant advantage of this approach is the straightforward data collection process.
Users can easily gather data for various hand gestures, which can then be incorporated into the system.
The flexibility to add new gestures allows for customization based on specific needs or preferences.
However, each new gesture requires a separate model to be trained. This means that a new dataset must be created for every additional gesture, and the model must undergo a training process to learn to recognize the new gesture accurately.

A larger dataset could significantly enhance the system's robustness.
By incorporating more data, the model can learn to recognize a broader range of variations in human hands.
These variations include differences in hand sizes, positions, and even physical disabilities.
A more extensive and diverse dataset would enable the model to generalize better, making it more adaptable to different users and scenarios.

Future enhancements could significantly improve the system's functionality and user experience.
One potential area of development is the detection of dynamic gestures, such as swiping, which would require the use of recurrent models.
These models are better suited for recognizing patterns in sequences, making them ideal for identifying gestures that involve movement over time.

Another critical aspect of future work is the speaker's determination to focus the system's attention on their hands.
This feature would be particularly useful in scenarios where multiple people are present, as it would prevent the system from mistakenly recognizing gestures from unintended individuals.
Techniques such as speaker identification or tracking algorithms could be employed to ensure that the system only responds to the designated presenter's hand gestures.

In summary, this proof of concept for "bare hand" controls offers a promising alternative to traditional presentation tools.
While the current implementation demonstrates the feasibility and potential of this approach, further development and enhancements could make it even more powerful and versatile, catering to a wider range of users and applications.

- The implemented approach is a proof of concept for “bare hand”-controls with no need for presenters etc.
- It achieves real time capabilities while making good predictions and can be used in presentations
- Data collection is simple and new gestures can be incorporated easily. However, new gestures require new models which need to be trained separately. A bigger dataset could be beneficial to cover variances of different human hands like hand sizes, positions or even disabilities.
- Deployment on Jetson Nano is tedious (and not necessary). Especially since it is labelled legacy hardware by Ultralytics :footcite:p:`ultralyticsNVIDIAJetson`. 
- In future work the detection of dynamic gestures like swiping could be implemented with recurrent models. Additionally, the speaker must be determined to focus only on their hands not to accidentally predict unwanted gestures.


Lessons Learned
---------------

Deploying and developing applications on edge devices presents unique challenges, primarily due to their limited computational power and compatibility issues with external software. Unlike a typical PC, edge devices often cannot support the full spectrum of software tools and libraries, requiring tailored solutions and optimizations to ensure efficient performance.

In data-driven projects, having a large and diverse dataset is crucial. More data allows for the incorporation of a wider range of gestures, enhancing the system's accuracy and versatility. Leveraging existing data sets can be advantageous, as collecting new data can be resource-intensive and sometimes unnecessary. Therefore, maximizing the use of pre-existing data is important. In the end, on very specialized projects collecting customized data is unavoidable.

The two-stage approach utilizing YOLO (You Only Look Once) and MobileNet proved to be effective. While initial solutions may seem suitable for the use case, exploring different methodologies can lead to more elegant and efficient solutions. For instance, the key point detection approach offered a more refined and sophisticated method for gesture recognition.

The project's performance could have been further enhanced with the use of more recent software distributions or more powerful hardware. As technology evolves, keeping the software and hardware up to date can significantly improve the system's accuracy and efficiency.

Time is a critical factor in any project. In this instance, we managed to implement a proof of concept within the available time frame, achieving promising results. However, with more time, there is potential for further refinements and enhancements to the system, highlighting the importance of time management and the need for iterative development cycles.

Overall, this project provided valuable insights into the challenges and considerations involved in developing and deploying applications on edge devices, the importance of data, and the benefits of exploring different technical approaches.


What can be improved on in the future?
--------------------------------------
Future improvements could focus on incorporating more dynamic gestures, such as "jump to slide x", "start/stop recording", and "set zoom level".
Detection of multiple hands and gestures involving multiple hands could be implemented to enhance functionality.
Additionally, increasing the variability of hand gestures for the same action would offer greater flexibility.
A "click" function for zooming in on slide windows could also be highly useful, allowing users to click on these windows using the laser pointer function.
Finally, the system should be ported to Windows and Mac so users can use it with their webcams instead of an external camera. This would make the system more accessible and easier to set up for a wider range of users.


.. footbibliography::