.. _model_implementation:

Model Implementation
====================
In this chapter, we will delve into the realized system and explore its various components and functionalities. Specifically, we will cover the following subchapters:

1. System Overview: This section provides an overview of the system architecture and the flow of data from the camera to the neural network.

2. Handling Unwanted Gesturing: Here, we discuss the countermeasures implemented to prevent accidental actions and reduce false positives in gesture recognition.

3. Data Collection: This subchapter explains the process of data collection using MediaPipe and the generation of hand landmarks for training the neural network.

4. Training: In this section, we describe the training process of the neural network, including the input data and the architecture of the network.

5. Evaluation: Here, we evaluate the performance of the trained model, including inference speeds and the overall system performance.

6. Deployment: The final subchapter covers the deployment process of the model on the Jetson Nano, including the necessary configurations and dependencies.


Overview
--------
As in any Computer Vision task, the system starts off with the input of the Camera (:ref:`Figure 3 <SysOverview>`).
It is attached directly to the Jetson Nano.
The Frames captured are then forwarded to MediaPipe Hand Landmarker :footcite:p:`lugaresi_mediapipe_2019` which will try to detect the active presenters’ hands.
It does this by looking at the z-coordinate and choosing the closest hand found.
As a result, we receive a set of nodes and edges that represent the orientation of said hand and fingers.
The sensitivity of the model changes based on the number of frames it can run on.
In our case, a gesture is only detected after being recognized for 9 consecutive frames with a confidence of 0,97 each.
Moving on, the output from MediaPipe is then forwarded to the neural network.
It will try to assign the input to a predefined gesture.
It’s important to note that there is also an edge class that translates to no gesture being detected at any given time.
Once detected, predefined actions will be taken with the help of the pywin32 library :footcite:p:`noauthor_mhammondpywin32_nodate`. 
Since the pywin32 library only provides high-level controls for PowerPoint, such as moving to a specific slide or pausing the presentation, we needed an additional library to enable laser pointer movements.
To achieve this, we utilized the pyautogui library, which allows for precise control of the laser pointer's movements (through mouse movements) via coordinates, ensuring a comprehensive and seamless presentation experience. 

.. figure:: img/SystemOverview.png
   :align: center
   :name: SysOverview
   
   Figure 3: System overview


Data Collection 
---------------
MediaPipe already comes pretrained by Google :footcite:p:`noauthor_hand_nodate`.
They use around 30,000 images on different background settings.
The Hand Landmarker consists of two steps.
First, it detects individual hands.
From there, it generates 21 key points that resemble the knuckles, joints, and tips of each finger (:ref:`Figure 4 <HandLandmarks>`).

.. figure:: img/HandLandmarks.png
   :align: center
   :name: HandLandmarks
   :class: custom-max-width-50
   
   Figure 4: MediaPipe Hand Landmark keypoints :footcite:p:`noauthor_hand_nodate`

Thanks to MediaPipe, our neural network does not have to train on actual images, making training easier, more reliable, and faster.
This way, we bypass the problem of, for example, variability in lighting or camera resolution.
To collect data to train our neural network, we wrote a :ref:`script <img_gen>` that allows you to automatically capture 100 images per gesture, one after the other, and save them in the corresponding file path.
Using MediaPipe, we extract 21 key points per image, each with 3 coordinates (x, y, and z), which will be saved as an array.
The x and y coordinates represent the position in the dimensions of the image.
The z-coordinate, on the other hand, describes how close the landmark is to the camera.
This way, we can perceive depth and derive the active presenter's hand used for gesture control.
Remember that we decided on one-handed gestures, so the training data consists of a collection of landmarks for one hand for each gesture.
This allows individual classification per hand later if there are several hands.
Summing up, each data point contains 63 numerical values and a label with the actual class it belongs to.
The neural network will choose the class it deems most likely.
There are 5 classes: *swipe_left*, *swipe_right*, *laser pointer*, *stop* and *edge* if no gesture was detected. 
The first 4 classes are self-explanatory as they correspond to the predefined gestures.
The *edge*-class is used to recognize and intercept various gestures that are close to the other gestures and can occur subconsciously when presenting.
When a gesture is classified to the *edge*-class, it will not start an action.
In total, we used 800 images per class for training, which sums up to 4.000 images total.
One quarter of these were captured in the same room where the final presentation was held to fine-tune the model.
A MediaPipe detection confidence value of 0.5 was used for training. 

Handling unwanted gesturing
---------------------------
Given the nature of our application, where accidental actions such as unintentionally switching to the next slide can cause significant disruptions, we implemented several countermeasures.
These countermeasures, briefly mentioned in the overview above, played a crucial role in our learning process and are essential for ensuring the system's reliability and are therefore highlighted here again:

1. **Edge Class**: We introduced an edge class that includes all edge cases and normal hand movements. For example, in the first model tests, we saw that just tilting your hand could trigger the next slide gesture without the need to stick out the index finger. We then included such cases in this class to force the model to predict the gestures only when required. This additional class helps the model to distinguish between actual gestures and unintended hand movements, reducing false positives. 
2. **Confidence**: We set a high confidence threshold, where a gesture is only considered valid if the model's output confidence exceeds 0.97. This strict criterion helps ensure that only highly certain gestures trigger actions.
3. **Sliding Window**: To further ensure that only valid gestures are recognized we implemented a sliding window mechanism, requiring a gesture to consistently be detected over a horizon of 6 frames with the mentioned confidence level. This method proved to be more effective than our initial approach, which used a majority vote over the frame window, in reducing false positives.


Training
--------
Now that we have collected the aforementioned data, we can begin to train our neural network.
As an input we use the 63 numerical values, which are the 21 key points with their x, y, and z coordinates.
The neural network consists of an instance norm and 3 feed-forward layers, each followed by a ReLU, except for the last layer, which uses a softmax activation function (:ref:`Figure 5 <NN>`).
The instance norm is used to normalize the inputs across the x, y, and z components so that the hand’s distance to the camera (i.e., the magnitude of the inputs) does not matter to increase robustness and facilitate more stable learning.
A grid search was run to explore different sets of hyperparameters.
A leaning rate of 0,001 for 200 epochs delivered the best results.
Another question we had to ask ourselves was how confident the neural network must be to output a gesture that will trigger an action.
Through experimentation, we fine-tuned how confident the system needs to be before it can output anything but the *edge*-class.

.. figure:: img/NN.png
   :align: center
   :name: NN
   
   Figure 5: Schematic representation of the neural network

Training Evaluation
~~~~~~~~~~~~~~~~~~~

As MediaPipe Landmark Detection is a well-working and reliable model we focus on the neural network as well as the whole process for the evaluation.
To ensure the robustness and reliability of our neural network for gesture recognition, we carefully balanced our dataset during the data collection phase.
This balanced dataset was crucial in achieving high accuracy, as it ensured that the model had an equal representation of each gesture class, preventing bias towards any particular gesture.

We used accuracy as our primary metric since have a balanced dataset. 
The effectiveness of this approach is reflected in our accuracy metrics:
- Training Accuracy: 98.6%
- Test Accuracy: 98.8%

For a more detailed evaluation, we also created a confusion matrix (:ref:`Figure 6 <CM>`).
These high accuracy rates indicate that our neural network performs well not only on the training data but also on unseen test data, demonstrating its ability to generalize effectively.
The test set was separated before training to ensure unbiased evaluation.
However, due to the nature of our data collection script, which captures many images in quick succession, some correlation between training and test data was inevitable.
This correlation underscores the importance of further qualitative validation to assess the overall approach comprehensively.

.. figure:: img/confusion_matrix.png
   :align: center
   :name: CM
   :class: custom-max-width-75
   
   Figure 6: Confusion matrix


Deployment
----------
Before deploying our model, it was necessary to configure the Jetson Nano.
After flashing the device, it was equipped with Python and the required libraries.
The Python packages for the Jetson Nano differ from those used on conventional laptops due to its ARM-based architecture (aarch64) as opposed to the more common x86_64 architectures.
The following components are required to run our Gesture-Controlled Presentation System: MediaPipe (note that TensorFlow installation in ARM package is not necessary), OpenCV, and PyTorch.
The OpenCV package was built from source so that it could be built with GStreamer support, which is essential to use the built-in Jetson Nano camera.
Archiconda and virtual environments are supported, albeit with some limitations.
Installing OpenCV with GStreamer support within these environments proved challenging.
Consequently, we installed the latest Arch versions directly into the user space.
With the setup complete, we can utilize the socket on the Jetson Nano to communicate with and control PowerPoint.
Installed packages (versions):

1. `Mediapipe <https://ai.google.dev/edge#mediapipe>`_ (0.8.5) 
2. `OpenCV <https://opencv.org>`_ (4.8.0) 
3. `Pytorch <https://pytorch.org>`_ (1.11)

Communication between Jetson Nano and Laptop 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
To deploy the gesture-controlled presentation system, it is essential to establish effective communication between the Jetson Nano, which processes the images and predicts gestures based on the detected hand key points, and the device responsible for controlling and displaying the PowerPoint presentation.
The data sent from the server includes gesture labels and serialized hand key points in string format.

The Jetson Nano, acting as the server, sends the gesture data to the client.
The laptop, serving as the client, receives the gesture data and controls the PowerPoint presentation based on the received gestures.
The communication protocol used is TCP/IP (Transmission Control Protocol/Internet Protocol), providing bi-directional data transmission over a reliable stream of bytes.
The connection is established with the Jetson Nano listening on a specific IP and port and the laptop connecting to this endpoint.
TCP ensures reliable data transmission with error checking and retransmission, making it highly reliable.
This setup enables real-time interaction between the server and the client, which is essential for responsive gesture control.
Standard libraries and APIs available for socket programming in Python simplify development, making it easy to implement.

While this approach offers several advantages, such as reliability, error checking, and retransmission, it also has some downsides, such as the need for handling connection establishment and managing connection issues.

In our use case, both the server (Jetson Nano) and client (Laptop) are connected to the same router, facilitating local network communication.
This setup ensures lower latency and higher data transfer rates compared to internet-based communication.

Inference speeds
~~~~~~~~~~~~~~~~
When evaluating the performance of our gesture recognition system, inference speed is a critical factor, especially in real-time applications.
We measured the inference speeds under different conditions to assess how efficiently the system operates.
The speed measurements were conducted on the Jetson Nano, covering the entire process from start to finish.
This includes receiving the camera input frame (which has a resolution of 1920x1080 and is recorded at up to 30 FPS), using MediaPipe to detect the hand key points, determining which hand is the active one, utilizing the neural network (NN) to classify the gesture and output the confidence level, adding the gesture to the frame horizon when the confidence level is sufficiently high, and finally, sending the detected gesture (if it persists over six frames) along with the coordinates of the hand key points to the laptop.
This comprehensive measurement ensures that the recorded speeds accurately reflect the complete workflow, providing a realistic assessment of the system's performance in real-time usage scenarios.
We took a random sample of 53 iterations for both cases presented below and computed the average for the evaluation.

1. **No Hand Detected**: When no hand is present in the frame, the system performs inference with an average time of 0.044 seconds or 44 ms per frame, translating to approximately 23.21 frames per second (FPS). This indicates a high processing speed when the system is idle or when no gesture is being actively recognized.
2. **Hand Detected**: During the detection of a hand, the average inference time increases to 0.092 seconds or 92 ms per frame, resulting in an average of 11.07 FPS. 

The variation in inference speeds highlights the extra processing required during hand detection.
While the system performs efficiently at 20.48 FPS when no hand is detected, the 8.16 FPS during hand detection shows that more computational resources are needed.
In summary, the system's ability to achieve 49 ms per frame in idle conditions and 124 ms per frame during hand detection demonstrates good performance.
These speeds are more than sufficient for controlling PowerPoint presentations, ensuring that gestures are recognized and executed reliably, even if further enhancements could help maintain responsiveness and accuracy in more demanding real-world scenarios.

Holistic Evaluation
-------------------
Our goal is to test not just the model itself but to ensure that the entire system—gesture classification, gesture forwarding, and action execution—works smoothly in the final product.
This comprehensive testing is crucial for verifying that everything from recognizing gestures to triggering actions performs as expected.
We also need to see how well our countermeasures against false positives hold up in real-world conditions.
Testing under various lighting, environmental settings, and different distances from the camera helps us understand how the system handles these variables.
For example, we've found that challenging conditions, like direct sunlight coming from behind the presenter, can impact MediaPipe Landmark Detection.
However, one of the key benefits of using hand key points for gesture classification is that once MediaPipe reliably detects these key points, the system is not affected by background colors or lighting conditions.
This makes our gesture recognition more reliable, even in tricky scenarios.
For the validation process, we set up our Jetson Nano on a standing table, like you would have at the front of most conference rooms.
We gesticulated with the lights switched on and off.
In Addition, we alternated between a distance of 1.5 m and 3m from the camera. 
We had 4 persons of our group control a presentation with 30 consecutive actions each.
The gesture-sequence was randomly generated.
A “stop presentation mode”-gesture is always followed by another one to start presentation mode again.
While testing we could not get the system to initiate a false action.
A gesture would not be deemed as executed if either the system did not recognize the gesture at all or too late, breaking the natural flow of hand movement.
In all 120 cases we had the second case happen twice.
Interestingly this was on the same person and gesture.
We concluded that the cause for this was the angle the hand was held at in comparison to the camera.
After this revelation it did not happen again.
All in all, testing concluded an accuracy of 98,33%. Gesture controls felt smooth and reactive, especially the laser pointer. 

.. footbibliography::