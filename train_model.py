import pickle

import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, TensorDataset, random_split
from model import SimpleNN


# Function to calculate accuracy
def calculate_accuracy(model, data_loader):
    """
    Calculates the accuracy of a given model on a given data loader.

    Args:
        model (SimpleNN): The model to evaluate.
        data_loader (torch.utils.data.DataLoader): The data loader containing the test data.

    Returns:
        float: The accuracy of the model on the test data.
    """
    model.eval()
    correct = 0
    total = 0
    with torch.no_grad():
        for batch_data, batch_labels in data_loader:
            outputs = model(batch_data)
            _, predicted = torch.max(outputs.data, 1)
            total += batch_labels.size(0)
            correct += (predicted == batch_labels).sum().item()
    return correct / total


# Function to train the model
def train(model, train_loader, test_loader, criterion, optimizer, num_epochs):
    """
    Trains the given model using the provided data loaders, criterion, optimizer, and number of epochs.

    Args:
        model (torch.nn.Module): The model to be trained.
        train_loader (torch.utils.data.DataLoader): The data loader for the training dataset.
        test_loader (torch.utils.data.DataLoader): The data loader for the testing dataset.
        criterion (torch.nn.Module): The loss function used for training.
        optimizer (torch.optim.Optimizer): The optimizer used for training.
        num_epochs (int): The number of epochs to train the model.

    Returns:
        None
    """
    for epoch in range(num_epochs):
        model.train()
        for batch_data, batch_labels in train_loader:
            optimizer.zero_grad()
            outputs = model(batch_data)
            loss = criterion(outputs, batch_labels)
            loss.backward()
            optimizer.step()

        train_accuracy = calculate_accuracy(model, train_loader)
        test_accuracy = calculate_accuracy(model, test_loader)

        print(
            f"Epoch [{epoch+1}/{num_epochs}], Train Accuracy: {train_accuracy:.4f}, Test Accuracy: {test_accuracy:.4f}"
        )


def train_loop():
    """
    Trains a simple neural network model using the provided dataset.

    This function loads a dataset from a pickle file, preprocesses the data, splits it into training and test sets,
    initializes a neural network model, defines the loss function and optimizer, and trains the model using the training set.
    The trained model is then saved to a pickle file.

    Args:
        None

    Returns:
        None
    """
    # Load the dataset from the pickle file
    with open("data.pickle", "rb") as file:
        dataset_dict = pickle.load(file)

    # Access the data and labels
    data = pd.DataFrame(dataset_dict["data"])
    labels = pd.DataFrame(dataset_dict["labels"])

    # Convert data to PyTorch tensors
    data_tensor = torch.tensor(data.values, dtype=torch.float32)
    labels_tensor = torch.tensor(labels.values, dtype=torch.long).squeeze()

    # Split the data into training and test sets (80-20 split)
    dataset = TensorDataset(data_tensor, labels_tensor)
    train_size = int(0.8 * len(dataset))
    test_size = len(dataset) - train_size
    train_dataset, test_dataset = random_split(dataset, [train_size, test_size])

    train_loader = DataLoader(train_dataset, batch_size=32, shuffle=True)
    test_loader = DataLoader(test_dataset, batch_size=32, shuffle=False)

    # Initialize the model, loss function, and optimizer
    input_size = data_tensor.shape[1]
    num_classes = labels_tensor.unique().size(0)
    model = SimpleNN(input_size, num_classes)

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=0.001)

    # Train the model
    num_epochs = 200
    train(model, train_loader, test_loader, criterion, optimizer, num_epochs)

    # Save the model to a pickle file
    with open("model.p", "wb") as f:
        pickle.dump({"model": model.state_dict()}, f)


if __name__ == "__main__":
    train_loop()
