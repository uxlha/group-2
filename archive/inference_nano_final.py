# IMPORTS
import os
import pickle
import numpy as np
import cv2
import mediapipe as mp
import torch
import torch.nn as nn
import torch.nn.functional as F
# import pyautogui

# pyautogui.FAILSAFE = False

DISPLAY_WIDTH = 2880
DISPLAY_HEIGHT = 1800

gestures = ["edge", "laser pointer", "swipe_left", "swipe right", "stop"]
commands = ["", "", "left", "right", "B"]

gesture_command_dict = dict(zip(gestures, commands))

# Initialize MediaPipe drawing utils
mp_drawing = mp.solutions.drawing_utils

# Function to load the saved model
def load_model(model_path):
    with open(model_path, 'rb') as file:
        model_dict = pickle.load(file)
    model_state = model_dict['model']

    # Initialize the model architecture
    input_size = model_state['fc1.weight'].size(1)
    num_classes = model_state['fc3.bias'].size(0)
    model = SimpleNN(input_size, num_classes)
    model.load_state_dict(model_state)

    return model

# Define the neural network model
class SimpleNN(nn.Module):
    def __init__(self, input_size, num_classes):
        super(SimpleNN, self).__init__()
        self.fc1 = nn.Linear(input_size, 128)
        self.fc2 = nn.Linear(128, 64)
        self.fc3 = nn.Linear(64, num_classes)

    def forward(self, x):
        x = F.relu(self.fc1(x))    # ReLU activation for the first hidden layer
        x = F.relu(self.fc2(x))    # ReLU activation for the second hidden layer
        x = self.fc3(x)            # Output layer without activation function
        x = F.softmax(x, dim=1)    # Softmax activation for multi-class classification
        return x

# Load the saved model
model_path = 'model.p'
model = load_model(model_path)
model.eval()

# Initialize MediaPipe Hands
mp_hands = mp.solutions.hands
hands = mp_hands.Hands(static_image_mode=False, max_num_hands=2, min_detection_confidence=0.5)

# Define actions (ensure this matches your trained model)
ACTIONS = np.array(["edge", "laser pointer", "swipe_left", "swipe right", "stop"])

# Function to perform inference on camera frames
def classify_gesture(model, hands, confidence_threshold=0.8, consecutive_frames=10):
    pipeline = "nvarguscamerasrc sensor-id=0 ! video/x-raw(memory:NVMM),width=1920, height=1080,framerate=30/1, format=NV12 ! nvvidconv flip-method=2 ! video/x-raw,format=BGRx, width=1280, height=720, pixel-aspect-ratio=1/1 ! videoconvert ! video/x-raw,format=BGR ! appsink drop=1"
    
    cap = cv2.VideoCapture(pipeline, cv2.CAP_GSTREAMER)
    consecutive_count = 0
    last_gesture = None

    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break

        frame = cv2.flip(frame, 1)  # Flip the frame horizontally

        img_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # Convert to RGB

        results = hands.process(img_rgb)  # Process the image with MediaPipe

        if results.multi_hand_landmarks:
            hand_data_list = []
            avg_z_list = []
            for hand_landmarks in results.multi_hand_landmarks:
                # Draw hand landmarks on the frame
                mp_drawing.draw_landmarks(frame, hand_landmarks, mp_hands.HAND_CONNECTIONS)

                hand_data = []
                avg_z = 0

                for landmark in hand_landmarks.landmark:
                    x = landmark.x
                    y = landmark.y
                    z = landmark.z if landmark.HasField("z") else 0
                    hand_data.extend([x, y, z])
                    avg_z += z

                avg_z /= 21
                hand_data_list.append(hand_data)
                avg_z_list.append(avg_z)

                # Determine the closer hand based on average z value (smaller z means closer)
            closer_hand_index = np.argmin(avg_z_list)
            closer_hand_data = hand_data_list[closer_hand_index]

            hand_data = np.array(closer_hand_data, dtype=np.float32)
            hand_data_tensor = torch.tensor(hand_data, dtype=torch.float32).unsqueeze(0)

            with torch.no_grad():
                outputs = model(hand_data_tensor)
                confidence, predicted = torch.max(outputs, 1)
                outputs[0, 0] = outputs[0, 0] + 0.1 if outputs[0, 0] + 0.1 <= 1 else outputs[0, 0]

                gesture_label = ACTIONS[predicted.item()]

                # Display the gesture label on the screen if confidence is above the threshold
                if confidence.item() > confidence_threshold and gesture_label != "edge":
                    cv2.putText(
                        frame,
                        f"{gesture_label} ({confidence.item():.2f})",
                        (10, 50),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        1,
                        (0, 255, 0),
                        2,
                        cv2.LINE_AA,
                    )
                    if(gesture_label == "laser pointer"):
                        print(f"Detected gesture: {gesture_label}")

                        if (last_gesture != "laser pointer"):
                            print("laser pointer")
                            # pyautogui.hotkey("command", "l")

                        # pyautogui.moveTo((int(hand_data[24] * DISPLAY_WIDTH / 2),int(hand_data[25] * DISPLAY_HEIGHT / 2)))

                        consecutive_count = 0  # Reset consecutive count
                    else:
                        if (last_gesture == "laser pointer"):
                            print("laser pointer")
                            # pyautogui.hotkey("command", "l")
                        # Check if the detected gesture is the same as last detected gesture
                        if gesture_label == last_gesture:
                            consecutive_count += 1
                        else:
                            consecutive_count = 0

                        # If the same gesture has been detected for consecutive_frames, print in console
                        if consecutive_count == consecutive_frames:
                            print(f"Detected gesture: {gesture_label}")
                            consecutive_count = 0  # Reset consecutive count
                            # pyautogui.hotkey(gesture_command_dict.get(gesture_label))

                    last_gesture = gesture_label
                else:
                    if (last_gesture == "laser pointer"):
                        print("laser pointer")
                        # pyautogui.hotkey("command", "l")
                    last_gesture = None

        cv2.imshow("Hand Gesture Classification", frame)  # Display the frame

        if cv2.waitKey(1) & 0xFF == ord("q"):  # Exit the loop if 'q' is pressed
            break

    cap.release()
    cv2.destroyAllWindows()

# Run the inference function
classify_gesture(model, hands, confidence_threshold=0.98, consecutive_frames=10)

