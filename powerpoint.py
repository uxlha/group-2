import win32com.client


class Presentation:

    def __init__(self, presentation_path) -> None:
        """
        Initializes a new instance of the Presentation class.

        Args:
            presentation_path (str): Path to the PowerPoint presentation.
        """
        self.presentation_path = presentation_path

    def start_powerpoint(self):
        """
        Initializes and opens Microsoft PowerPoint application with a specified presentation.

        Args:
            None

        Returns:
            powerpoint (win32com.client.Dispatch): PowerPoint application object.
            presentation (object): PowerPoint presentation object.
        """
        powerpoint = win32com.client.Dispatch("PowerPoint.Application")
        powerpoint.Visible = True
        presentation = powerpoint.Presentations.Open(self.presentation_path)
        self.powerpoint = powerpoint
        self.presentation = presentation

    def start_presentation_mode(self):
        """
        Activates the presenter view mode in PowerPoint.

        Args:
            None

        Returns:
            None
        """
        self.presentation.SlideShowSettings.ShowPresenterView = True
        self.presentation.SlideShowSettings.Run()

    def end_presentation(self):
        """
        Ends the PowerPoint presentation.

        Args:
            None

        Returns:
            None
        """
        self.powerpoint.SlideShowWindows(1).View.Exit()

    def hold_presentation(self):
        """
        Puts the PowerPoint presentation on hold by either returning to the current slide or turning the screen black.

        Args:
            None

        Returns:
            None
        """
        if self.powerpoint.SlideShowWindows(1).View.State == 3:
            self.powerpoint.SlideShowWindows(1).View.State = 1  # Return to the current slide
        else:
            self.powerpoint.SlideShowWindows(1).View.State = 3  # Turn the screen black

    def activate_laser_pointer(self, laserpointer_activated):
        """
        Activates or deactivates the laser pointer in PowerPoint.

        Args:
            laserpointer_activated (bool): True to activate laser pointer, False to deactivate.

        Returns:
            None
        """
        slide_show_window = self.presentation.SlideShowWindow
        slide_show_view = slide_show_window.View
        slide_show_view.LaserPointerEnabled = laserpointer_activated

    def next_slide(self):
        """Advances to the next slide in the presentation.

        This method uses the SlideShowWindow.View.Next() method to navigate to the next slide in the presentation.

        Args:
            None

        Returns:
            None
        """
        self.presentation.SlideShowWindow.View.Next()

    def previous_slide(self):
        """Goes to the previous slide in the PowerPoint presentation.

        This method navigates to the previous slide in the current PowerPoint presentation.

        Args:
            None

        Returns:
            None
        """
        self.presentation.SlideShowWindow.View.Previous()
