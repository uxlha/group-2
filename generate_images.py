# IMPORTS
import os
import numpy as np
import cv2


def create_folders():
    """
    Creates folders for storing images.

    This function creates folders for storing images based on the given actions.
    It checks if there are existing directories for each action and determines the number of existing images.
    If there are existing directories, it starts from the next index.
    If there are no existing directories, it starts from scratch.

    Parameters:
        None
    Returns:
        None
    """
    for action_ind, action in enumerate(ACTIONS):
        action_path = os.path.join(DATA_PATH, action)
        if not os.path.exists(action_path):
            os.makedirs(action_path)
            num_existing_images[action_ind] = 0
            print(f"No existing data directory found for action {action}. Starting from scratch.")
        else:
            num_existing_images[action_ind] = len(
                [name for name in os.listdir(action_path) if os.path.isfile(os.path.join(action_path, name))]
            )
            print(f"Already found {num_existing_images[action_ind]} images for action {action}.")


def create_data():
    """
    Captures video frames and saves them as images for each action.

    This function captures video frames from the default camera and saves them as images for each action.
    It captures 100 images for each action and saves them in the specified data path.

    Args:
        None

    Returns:
        None
    """
    cap = cv2.VideoCapture(0)

    for action_ind, action in enumerate(ACTIONS):
        while True:
            ret, frame = cap.read()
            frame = cv2.flip(frame, 1)
            cv2.putText(
                frame,
                f'Press "Space" to start recording images for class {action}',
                (50, 50),
                cv2.FONT_HERSHEY_SIMPLEX,
                1.0,
                (255, 255, 255),
                2,
                cv2.LINE_AA,
            )
            cv2.imshow("frame", frame)
            if cv2.waitKey(25) == ord(" "):
                break

        print(f"Capturing images for action: {action}")
        for img_num in range(100):
            ret, frame = cap.read()
            frame = cv2.flip(frame, 1)
            cv2.putText(
                frame,
                f"Recording {action} ({img_num + 1}/100)",
                (50, 50),
                cv2.FONT_HERSHEY_SIMPLEX,
                1.0,
                (0, 255, 0),
                2,
                cv2.LINE_AA,
            )
            cv2.imshow("frame", frame)
            cv2.waitKey(25)
            image_path = os.path.join(
                DATA_PATH, action, "{}.jpg".format(str(num_existing_images[action_ind] + img_num).zfill(5))
            )
            cv2.imwrite(image_path, frame)

        num_existing_images[action_ind] += 100

    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    DATA_PATH = os.path.join("DATA")
    ACTIONS = np.array(["edge", "laser pointer", "swipe_left", "swipe right", "stop"])

    num_existing_images = [0] * len(ACTIONS)

    create_folders()
    create_data()
