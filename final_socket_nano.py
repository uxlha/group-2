# Import necessary libraries
import numpy as np
import cv2
import mediapipe as mp
import torch
import socket
from model import ModelFactory

# Additionally for testing inference speed
import time

# List of gestures recognized by the model
gestures = ["edge", "laser pointer", "swipe_left", "swipe_right", "stop"]

# Initialize MediaPipe drawing utilities
mp_drawing = mp.solutions.drawing_utils


def serialize_landmarks(landmarks):
    """
    Serializes a list of landmarks into a string format.

    Args:
        landmarks (list): A list of landmarks.

    Returns:
        str: A string representation of the serialized landmarks.
    """
    serialized = ",".join(map(str, landmarks))
    return serialized


# Function to perform gesture classification
def classify_gesture(model, hands, conn, confidence_threshold=0.97, consecutive_frames=6):
    """
    Performs real-time gesture classification using the provided model and MediaPipe Hands.

    Args:
        model (SimpleNN): Trained neural network model.
        hands (mp.solutions.hands.Hands): MediaPipe Hands object for hand tracking.
        conn (socket.socket): Socket connection object for sending gesture data.
        confidence_threshold (float, optional): Minimum confidence level for gesture classification. Defaults to 0.97.
        consecutive_frames (int, optional): Number of consecutive frames for gesture recognition stability. Defaults to 6.

    Raises:
        BrokenPipeError: If the connection with the client is broken.

    Returns:
        None
    """
    # Pipeline configuration for capturing camera frames
    pipeline = "nvarguscamerasrc sensor-id=0 ! video/x-raw(memory:NVMM),width=1920, height=1080,framerate=30/1, format=NV12 ! nvvidconv flip-method=2 ! video/x-raw,format=BGRx, width=1280, height=720, pixel-aspect-ratio=1/1 ! videoconvert ! video/x-raw,format=BGR ! appsink drop=1"

    # Open video capture
    cap = cv2.VideoCapture(pipeline, cv2.CAP_GSTREAMER)

    # List of gesture labels
    ACTIONS = np.array(["edge", "laser pointer", "swipe_left", "swipe_right", "stop"])

    consecutive_count = 0  # Counter for consecutive detections of the same gesture
    last_gesture = None  # Variable to store the last detected gesture

    try:
        while cap.isOpened():
            start_time = time.time()  # Start timing (inference speed)

            ret, frame = cap.read()
            if not ret:
                break

            frame = cv2.flip(frame, 1)  # Flip the frame horizontally

            img_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # Convert to RGB

            # Process the image with MediaPipe Hands
            results = hands.process(img_rgb)
            mp_hands = mp.solutions.hands
            data = f"none/{0}/{0}/no_hand"
            if results.multi_hand_landmarks:
                hand_data_list = []
                avg_z_list = []
                for hand_landmarks in results.multi_hand_landmarks:
                    # Draw hand landmarks on the frame
                    mp_drawing.draw_landmarks(frame, hand_landmarks, mp_hands.HAND_CONNECTIONS)

                    hand_data = []
                    avg_z = 0

                    for landmark in hand_landmarks.landmark:
                        x = landmark.x
                        y = landmark.y
                        z = landmark.z if landmark.HasField("z") else 0
                        hand_data.extend([x, y, z])
                        avg_z += z

                    avg_z /= 21
                    hand_data_list.append(hand_data)
                    avg_z_list.append(avg_z)

                # Determine the closer hand based on average z value (smaller z means closer)
                closer_hand_index = np.argmin(avg_z_list)
                closer_hand_data = hand_data_list[closer_hand_index]

                hand_data = np.array(closer_hand_data, dtype=np.float32)
                hand_data_tensor = torch.tensor(hand_data, dtype=torch.float32).unsqueeze(0)

                data_str_keypoints = serialize_landmarks(hand_data)

                with torch.no_grad():
                    outputs = model(hand_data_tensor)
                    confidence, predicted = torch.max(outputs, 1)

                    outputs[0, 0] = outputs[0, 0] + 0.05 if outputs[0, 0] + 0.05 <= 1 else outputs[0, 0]

                    gesture_label = ACTIONS[predicted.item()]
                    data = f"none/{hand_data[24]}/{hand_data[25]}/{data_str_keypoints}"
                    # Display the gesture label on the screen if confidence is above the threshold
                    if confidence.item() > confidence_threshold and gesture_label != "edge":
                        cv2.putText(
                            frame,
                            f"{gesture_label} ({confidence.item():.2f})",
                            (10, 50),
                            cv2.FONT_HERSHEY_SIMPLEX,
                            1,
                            (0, 255, 0),
                            2,
                            cv2.LINE_AA,
                        )
                        if gesture_label == "laser pointer":
                            print(f"Detected gesture: {gesture_label}")

                            if last_gesture != "laser pointer":
                                data = f"{gesture_label}/{hand_data[24]}/{hand_data[25]}/{data_str_keypoints}"

                            else:
                                data = f"reposition laser/{hand_data[24]}/{hand_data[25]}/{data_str_keypoints}"

                            consecutive_count = 0  # Reset consecutive count
                        else:
                            if last_gesture == "laser pointer":
                                data = f"deactivate laser pointer/{hand_data[24]}/{hand_data[25]}/{data_str_keypoints}"

                            if gesture_label == last_gesture:
                                consecutive_count += 1
                            else:
                                consecutive_count = 0

                            if consecutive_count == consecutive_frames:
                                print(f"Detected gesture: {gesture_label}")
                                consecutive_count = 0  # Reset consecutive count
                                data = f"{gesture_label}/{hand_data[24]}/{hand_data[25]}/{data_str_keypoints}"

                        last_gesture = gesture_label
                    else:
                        if last_gesture == "laser pointer":
                            data = f"deactivate laser pointer/{hand_data[24]}/{hand_data[25]}/{data_str_keypoints}"

                        last_gesture = None
            conn.sendall(data.encode("utf-8"))

            #cv2.imshow("Hand Gesture Classification", frame)  # Display the frame

            if cv2.waitKey(1) & 0xFF == ord("q"):  # Exit the loop if 'q' is pressed
                break

            # Measure inference time

            end_time = time.time()
            inference_time = end_time - start_time
            fps = 1 / inference_time
            print(f"Inference Time: {inference_time:.4f} seconds, FPS: {fps:.2f}")

    except BrokenPipeError:
        print("Connection with client broken. Waiting for new connection...")

    cap.release()
    cv2.destroyAllWindows()


# Main function to run the server
def main():
    """Runs the main server loop for gesture classification.

    This function initializes the necessary components for gesture classification, including loading a saved model,
    initializing MediaPipe Hands, and creating a socket object for communication. Using this socket object,
    the server listens to incoming connections and connects to the client (i.e., the laptop running the power
    point presentation). It then enters a loop to perform gesture classification in real-time and send the detected
    gestures to the client.


    Returns:
        None
    """
    # Define the IP address and port to listen on
    HOST = "0.0.0.0"  # Listen on all available interfaces
    PORT = 12345  # Port for connection

    # Load the saved model
    model_path = "model.p"
    model = ModelFactory.load_model(model_path)
    model.eval()

    # Initialize MediaPipe Hands
    mp_hands = mp.solutions.hands
    hands = mp_hands.Hands(static_image_mode=False, max_num_hands=2, min_detection_confidence=0.5)

    # Create a socket object
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # Bind the socket to the host and port
        server_socket.bind((HOST, PORT))

        while True:
            try:
                # Start listening for incoming connections
                server_socket.listen()
                print(f"Server listening on {HOST}:{PORT}")

                # Accept a client connection
                conn, addr = server_socket.accept()
                with conn:
                    print(f"Connected by {addr}")

                    # Perform gesture classification
                    classify_gesture(
                        model=model, hands=hands, conn=conn, confidence_threshold=0.97, consecutive_frames=9
                    )

            except KeyboardInterrupt:
                print("Keyboard interrupt detected. Exiting...")
                break


if __name__ == "__main__":
    main()
