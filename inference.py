import numpy as np
import cv2
import mediapipe as mp
import torch
import pyautogui
from model import ModelFactory


# Function to perform inference on camera frames
def classify_gesture(model, hands, confidence_threshold=0.8, consecutive_frames=10):
    """
    Classifies hand gestures in real-time using a trained model.

    Parameters:
        model (torch.nn.Module): The trained model for gesture classification.
        hands (mediapipe.solutions.hands.Hands): The MediaPipe Hands object for hand tracking.
        confidence_threshold (float, optional): The minimum confidence threshold for gesture classification.
            Defaults to 0.8.
        consecutive_frames (int, optional): The number of consecutive frames required to detect the same gesture.
            Defaults to 10.

    Returns:
        None
    """
    cap = cv2.VideoCapture(0)
    consecutive_count = 0
    last_gesture = None

    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break

        frame = cv2.flip(frame, 1)  # Flip the frame horizontally

        img_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # Convert to RGB

        results = hands.process(img_rgb)  # Process the image with MediaPipe

        if results.multi_hand_landmarks:
            hand_data_list = []
            avg_z_list = []
            for hand_landmarks in results.multi_hand_landmarks:
                # Draw hand landmarks on the frame
                mp_drawing.draw_landmarks(frame, hand_landmarks, mp_hands.HAND_CONNECTIONS)

                hand_data = []
                avg_z = 0

                for landmark in hand_landmarks.landmark:
                    x = landmark.x
                    y = landmark.y
                    z = landmark.z if landmark.HasField("z") else 0
                    hand_data.extend([x, y, z])
                    avg_z += z

                avg_z /= 21
                hand_data_list.append(hand_data)
                avg_z_list.append(avg_z)

                # Determine the closer hand based on average z value (smaller z means closer)
            closer_hand_index = np.argmin(avg_z_list)
            closer_hand_data = hand_data_list[closer_hand_index]

            hand_data = np.array(closer_hand_data, dtype=np.float32)
            hand_data_tensor = torch.tensor(hand_data, dtype=torch.float32).unsqueeze(0)

            with torch.no_grad():
                outputs = model(hand_data_tensor)
                confidence, predicted = torch.max(outputs, 1)
                outputs[0, 0] = outputs[0, 0] + 0.1 if outputs[0, 0] + 0.1 <= 1 else outputs[0, 0]

                gesture_label = ACTIONS[predicted.item()]

                # Display the gesture label on the screen if confidence is above the threshold
                if confidence.item() > confidence_threshold and gesture_label != "edge":
                    cv2.putText(
                        frame,
                        f"{gesture_label} ({confidence.item():.2f})",
                        (10, 50),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        1,
                        (0, 255, 0),
                        2,
                        cv2.LINE_AA,
                    )
                    if gesture_label == "laser pointer":
                        print(f"Detected gesture: {gesture_label}")

                        if last_gesture != "laser pointer":
                            pyautogui.hotkey("command", "l")

                        pyautogui.moveTo(
                            (int(hand_data[24] * DISPLAY_WIDTH / 2), int(hand_data[25] * DISPLAY_HEIGHT / 2))
                        )

                        consecutive_count = 0  # Reset consecutive count
                    else:
                        if last_gesture == "laser pointer":
                            pyautogui.hotkey("command", "l")
                        # Check if the detected gesture is the same as last detected gesture
                        if gesture_label == last_gesture:
                            consecutive_count += 1
                        else:
                            consecutive_count = 0

                        # If the same gesture has been detected for consecutive_frames, print in console
                        if consecutive_count == consecutive_frames:
                            print(f"Detected gesture: {gesture_label}")
                            consecutive_count = 0  # Reset consecutive count
                            pyautogui.hotkey(gesture_command_dict.get(gesture_label))

                    last_gesture = gesture_label
                else:
                    if last_gesture == "laser pointer":
                        pyautogui.hotkey("command", "l")
                    last_gesture = None

        cv2.imshow("Hand Gesture Classification", frame)  # Display the frame

        if cv2.waitKey(1) & 0xFF == ord("q"):  # Exit the loop if 'q' is pressed
            break

    cap.release()
    cv2.destroyAllWindows()


# Run the inference function
if __name__ == "__main__":
    pyautogui.FAILSAFE = False

    DISPLAY_WIDTH = 2880
    DISPLAY_HEIGHT = 1800

    gestures = ["edge", "laser pointer", "swipe_left", "swipe right", "stop"]
    commands = ["", "", "left", "right", "B"]

    gesture_command_dict = dict(zip(gestures, commands))

    # Initialize MediaPipe drawing utils
    mp_drawing = mp.solutions.drawing_utils

    # Load the saved model
    model_path = "model.p"
    model = ModelFactory.load_model(model_path)
    model.eval()

    # Initialize MediaPipe Hands
    mp_hands = mp.solutions.hands
    hands = mp_hands.Hands(static_image_mode=False, max_num_hands=2, min_detection_confidence=0.5)

    # Define actions (ensure this matches your trained model)
    ACTIONS = np.array(["edge", "laser pointer", "swipe_left", "swipe right", "stop"])
    classify_gesture(model, hands, confidence_threshold=0.98, consecutive_frames=10)
