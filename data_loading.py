# IMPORTS
import os
import pickle
import numpy as np
import mediapipe as mp
import cv2


def main():
    """
    The main function for loading and processing hand image data.

    This function loads hand images from the specified data path, processes them using the MediaPipe Hands library,
    and saves the processed data and corresponding labels to a pickle file.

    This scipt is used to generate the training data for the neural network to classify hand gestures.

    Returns:
        None
    """

    DATA_PATH = os.path.join("DATA")
    ACTIONS = np.array(["edge", "laser pointer", "swipe_left", "swipe right", "stop"])

    label_map = {label: num for num, label in enumerate(ACTIONS)}

    mp_hands = mp.solutions.hands
    hands = mp_hands.Hands(static_image_mode=True, min_detection_confidence=0.5, max_num_hands=1)

    data, labels = [], []

    for action in ACTIONS:
        action_path = os.path.join(DATA_PATH, action)
        image_files = [f for f in os.listdir(action_path) if os.path.isfile(os.path.join(action_path, f))]

        for img_file in image_files:
            img_path = os.path.join(action_path, img_file)
            img = cv2.imread(img_path)
            img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

            results = hands.process(img_rgb)
            if results.multi_hand_landmarks:
                for hand_landmarks in results.multi_hand_landmarks:
                    hand_data = []
                    for i in range(len(hand_landmarks.landmark)):
                        x = hand_landmarks.landmark[i].x
                        y = hand_landmarks.landmark[i].y
                        z = hand_landmarks.landmark[i].z

                        hand_data.append([x, y, z])

                hand_data = np.array(hand_data).flatten()
                data.append(hand_data)
            else:
                data.append(np.zeros(21 * 3))

            labels.append(label_map[action])

    print(np.array(data).shape)
    print(np.array(labels).shape)

    with open("data.pickle", "wb") as f:
        pickle.dump({"data": data, "labels": labels}, f)


if __name__ == "__main__":
    main()
